import sys
import cv2
from PIL import Image, ImageStat

def is_gray_scale(img, thumb_size=20, MSE_cutoff=350 , adjust_color_bias=True):
    img = img.copy()
    # crop and resize
    h, w = len(img), len(img[0])
    img = img[h//7:h*6//7][w//7:w*6//7]
    while (w*h > 80**2):
        img = cv2.resize(img, (w//2, h//2)) 
        h, w = len(img), len(img[0])
    h, w = len(img), len(img[0])
    
    thumb = Image.fromarray(img)
    SSE, bias = 0, [0,0,0]
    if adjust_color_bias:
        bias = ImageStat.Stat(thumb).mean[:3]
        bias = [b - sum(bias)/3 for b in bias ]
    for pixel in thumb.getdata():
        mu = sum(pixel)/3
        SSE += sum((pixel[i] - mu - bias[i])**2 for i in range(3))
    MSE = SSE/(h*w)
    if MSE <= MSE_cutoff:
        #print("grayscale, ")
        return True
    else:
        #print (file_name, MSE)
        return False

if __name__ == "__main__":
    img_name = sys.argv[1]
    img = cv2.imread(img_name)

    #cv2.imshow("win", img)
    #cv2.waitKey(0)

    is_gray_scale(img)
