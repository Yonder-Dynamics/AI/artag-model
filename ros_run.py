#!/usr/bin/env python3
import rospy
import serial, time
from cv_bridge import CvBridge, CvBridgeError
import message_filters

from get_bounding_box import get_bounding_box
from calc_pose import calc_pose

def callback(image_data, rover_pose_data):
    try:
      cv_image = bridge.imgmsg_to_cv2(image_data, "bgr8")
      rover_pose = rover_pose_data.pose
      boundingBoxes = get_bounding_box(detection_model, cv_image, True)
      pose = calc_pose(boundingBoxes, rover_pose)
      print(pose)
      ar_pose_pub.publish(pose)
    except CvBridgeError as e:
      print(e)

rospy.init_node('ar_detector')

bridge = CvBridge()
ar_pose_pub = rospy.Publisher('ar_pose', PoseArray, queue_size=1)
ar_image_pub = rospy.Publisher('/armodel/bboximg', Image, queue_size=1)

pose_sub = message_filters.Subscriber('/zed/zed_node/pose', PoseStamped)
image_sub = message_filters.Subscriber('/zed/zed_node/rgb/image_rect_color', Image)
ats = message_filters.ApproximateTimeSynchronizer([image_sub, pose_sub], queue_size=1, slop=0.2)
ats.registerCallback(callback)

rospy.loginfo("started ar tag detector")
rospy.spin()
