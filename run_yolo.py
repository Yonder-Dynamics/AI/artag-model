#!/usr/bin/env python3
import rospy
import serial, time
from cv_bridge import CvBridge, CvBridgeError
import message_filters
import cv2 as cv
import actionlib
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseArray
from functools import reduce
from darknet_ros_msgs.msg import BoundingBox, BoundingBoxes, CheckForObjectsAction, CheckForObjectsGoal
import datetime

thresh = .3
tile_scale = 1

class ImageTile:
    ### x, y are the coordinates of the top left corner of tile in the original image
    def __init__(self, image, x, y, tile_scale):
        self.image = image;
        self.x = x;
        self.y = y;
        self.tile_scale = tile_scale;

###
# args:
# image - image
# width, height - how many tiles to split the image into along each axis
# overlap - overlap of tiles. Should be the maximum dimension we expect a tag to
#   take in the image
# returns:
#   [ImageTile] - the tiles
###
def tile_image(image, width, height, overlap=150):
    tile_height = image.shape[0] / height + overlap / 2
    tile_width = image.shape[1] / width + overlap / 2
    y_stride = tile_height - overlap
    x_stride = tile_width - overlap
    tiles = []
    for y in range(height):
        for x in range(width):
            sub_img = image[y*y_stride:y*y_stride+tile_height,
                            x*x_stride:x*x_stride+tile_width]
            sub_img = cv.resize(sub_img, (tile_scale * tile_width, tile_scale * tile_height))
            tiles.append(ImageTile(sub_img, x*x_stride, y*y_stride, tile_scale))
    tiles.append(ImageTile(image, 0, 0, 1))
    return tiles

def yolo_detect(image):
    yolo_client = actionlib.SimpleActionClient('/darknet_ros/check_for_objects', CheckForObjectsAction)
    yolo_client.wait_for_server()
    goal = CheckForObjectsGoal(0, bridge.cv2_to_imgmsg(image, "rgb8"))
    yolo_client.send_goal(goal)
    yolo_client.wait_for_result()
    return yolo_client.get_result().bounding_boxes

def valid_bbox(bbox):
    return (bbox.id == "tag" and bbox.probability > thresh)

def cull_redundant(acc, bbox):
    for existing in acc:
        if IOU(existing, bbox) > .2:
            return acc
    return acc + [bbox]

def IOU(boxA, boxB):
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)
    # return the intersection over union value
    return iou

def process_image(message):
    try:
        cv_image = bridge.imgmsg_to_cv2(message, "rgb8")
        #cv_image = cv.resize(cv_image, (cv_image.shape[0]*2, cv_image.shape[1]*2), interpolation = cv.INTER_AREA)
        tiles = tile_image(cv_image, 3, 2)
        bboxes = []
        for tile in tiles:
            tile_boxes = yolo_detect(tile.image)
            print(datetime.datetime.now());
            for box in tile_boxes.bounding_boxes:
                print(box)
                box.xmin = box.xmin/tile.tile_scale + tile.x
                box.xmax = box.xmax/tile.tile_scale + tile.x
                box.ymin = box.ymin/tile.tile_scale + tile.y
                box.ymax = box.ymax/tile.tile_scale + tile.y
            bboxes.extend(tile_boxes.bounding_boxes)
        #bboxes = filter(valid_bbox, bboxes)
        #bboxes = reduce(cull_redundant, bboxes, [])
        print("-------")
        for bbox in bboxes:
            print((bbox.xmin, bbox.ymin), (bbox.xmax, bbox.ymax))
            cv_image = cv.rectangle(cv_image, (bbox.xmin, bbox.ymin), (bbox.xmax, bbox.ymax),
                          (255, 0, 0), 2)
        #print(bboxes)
        #cv.imwrite("tile.png", tiles[2].image)
        cv.imwrite("test.png", cv_image)
        #ar_image_pub.publish(bridge.cv2_to_imgmsg(cv_image, encoding="passthrough"))
    except CvBridgeError as e:
        print(e)

rospy.init_node('ar_detector')

bridge = CvBridge()
# ar_pose_pub = rospy.Publisher('ar_pose', PoseArray, queue_size=1)
ar_image_pub = rospy.Publisher('/armodel/bboximg', Image, queue_size=1)

image_sub = rospy.Subscriber('/zed/zed_node/rgb/image_rect_color', Image, process_image)
#print(yolo_detect(cv.imread("/home/yonder/ar-tag-detector/Data/resized/test/IMG_11.JPG")))

rospy.loginfo("started ar tag detector")
rospy.spin()
