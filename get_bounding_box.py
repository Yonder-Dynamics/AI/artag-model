import numpy as np
import cv2
import os
from pathlib import Path

# disable warnings
import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=FutureWarning)
    import tensorflow as tf
    from object_detection.utils import ops as utils_ops
    from object_detection.utils import label_map_util

from matchTmp import match
from classifyBW import is_gray_scale

######## Patches ########
# patch tf1 into `utils.ops`
utils_ops.tf = tf.compat.v1

# Patch the location of gfile
tf.gfile = tf.io.gfile

# allow multiple instances of the tensorflow stuff to run on the same GPU
physical_devices = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], True)

# disable future warnings
import warnings
warnings.filterwarnings("ignore")
######## end of path #########

def load_model(model_name):
    model_dir = os.path.join(os.path.dirname(__file__), "model/saved_model")

    model = tf.saved_model.load(model_dir)
    model = model.signatures['serving_default']

    return model

def run_inference_for_single_image(model, image):
    image = np.asarray(image)
    # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
    input_tensor = tf.convert_to_tensor(image)
    # The model expects a batch of images, so add an axis with `tf.newaxis`.
    input_tensor = input_tensor[tf.newaxis,...]

    # Run inference
    output_dict = model(input_tensor)

    # All outputs are batches tensors.
    # Convert to numpy arrays, and take index [0] to remove the batch dimension.
    # We're only interested in the first num_detections.
    num_detections = int(output_dict.pop('num_detections'))
    output_dict = {key:value[0, :num_detections].numpy()
                                 for key,value in output_dict.items()}
    output_dict['num_detections'] = num_detections

    # detection_classes should be ints.
    output_dict['detection_classes'] = output_dict['detection_classes'].astype(np.int64)

    # Handle models with masks:
    if 'detection_masks' in output_dict:
        # Reframe the the bbox mask to the image size.
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
                            output_dict['detection_masks'], output_dict['detection_boxes'],
                             image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(detection_masks_reframed > 0.5, tf.uint8)
        output_dict['detection_masks_reframed'] = detection_masks_reframed.numpy()

    return output_dict

#ymin(0), xmin(1), ymax(2), xmax(3)
def hasOverlap(box1, box2):
    if (box1[2] < box2[0] or
        box2[2] < box1[0] or
        box1[3] < box2[1] or
        box2[3] < box1[1]):
        return False
    return True

def area(box):
    return (box[3]-box[1])*(box[2]-box[0])

def intersection(box1, box2, thresh):
    if not hasOverlap(box1, box2):
        return False
    # has intersection at this point
    # want to calculate percentage
    b1yMin, b1xMin, b1yMax, b1xMax = box1
    b2yMin, b2xMin, b2yMax, b2xMax = box2
    width = min(b1xMax, b2xMax)-max(b1xMin, b2xMin)
    height = min(b1yMax, b2yMax)-max(b1yMin, b2yMin)
    percent = width*height/max(area(box1), area(box2))

    return (percent > thresh)


# templates from prev frame
templates = []
def get_bounding_box(model, image_np, show_img = False):
    # Actual detection.
    output_dict = run_inference_for_single_image(model, image_np)

    boxes = []
    # at this point the detection scores are sorted
    # get score and threshold to 80%
    for i, score in enumerate(output_dict['detection_scores']):
        if (score > 0.8):
            boxes.append(output_dict['detection_boxes'][i])
        else:
            break

    # match each template to the image
    global templates
    matchBoxes = []
    for template in templates:
        matchBoxes.append(match(image_np, template))

    tmpBoxes = boxes
    validBoxes = []
    for i, box in enumerate(tmpBoxes):
        for j, matchBox in enumerate(matchBoxes):
            if (intersection(box, matchBox, 0.7)):
                validBoxes.append(box)
                del matchBoxes[j]
                break

    #print(len(validBoxes))
    if (show_img):
        # Visualization of the results of a detection.
        img_for_show = image_np.copy()
        img_height, img_width = len(image_np), len(image_np[0])
        for box in validBoxes:
            top_left = (int(box[1]*img_width), int(box[0]*img_height))
            bottom_right = (int(box[3]*img_width), int(box[2]*img_height))
            cv2.rectangle(img_for_show, top_left, bottom_right, 255, 2)
        cv2.imshow("win", img_for_show)
        cv2.waitKey(1)

        #try:
        #    ar_image_pub.publish(bridge.cv2_to_imgmsg(img_for_show, "bgr8"))
        #except CvBridgeError as e:
        #    print(e)

    # update template
    templates = []
    for box in boxes:
        yMin, xMin, yMax, xMax = box
        yMin = int(yMin*img_height)
        yMax = int(yMax*img_height)
        xMin = int(xMin*img_width)
        xMax = int(xMax*img_width)
        template = image_np[yMin:yMax, xMin:xMax]
        if (is_gray_scale(template)):
            templates.append(template)
        #cv2.imwrite("templates/"+str(n)+".jpg", template)
        #n += 1

    return validBoxes

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join(os.path.dirname(__file__), "object_detection.pbtxt")
#PATH_TO_LABELS = str(Path.home())+"/artag-model/object_detection.pbtxt"
category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

model_name = 'ARTagModel'
detection_model = load_model(model_name)

if __name__ == "__main__":
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
    while(True):
        boundingBox = get_bounding_box(detection_model, cap.read()[1], True)
        #print(boundingBox)
