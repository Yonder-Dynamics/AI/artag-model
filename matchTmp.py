import cv2
import numpy as np
from matplotlib import pyplot as plt

def match(img, template, showImg=False):
    # Apply template Matching
    res = cv2.matchTemplate(img,template,cv2.TM_CCOEFF_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    top_left = max_loc
    h, w, _ = template.shape
    xmin = top_left[0]
    ymin = top_left[1]
    xmax = xmin+w
    ymax = ymin+h
    img_h, img_w, _ = img.shape

    if (showImg):
        bottom_right = (xmax, ymax)
        cv2.rectangle(img, top_left, bottom_right, 255, 2)

        plt.subplot(121),plt.imshow(res,cmap = 'gray')
        plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
        plt.subplot(122),plt.imshow(img,cmap = 'gray')
        plt.title('Detected Point'), plt.xticks([]), plt.yticks([])

        plt.show()

    return ymin/img_h, xmin/img_w, ymax/img_h, xmax/img_w

if (__name__ == "__main__"):
    img = cv2.imread('30.JPG')
    img2 = img.copy()
    template = cv2.imread('template.JPG')

    print(match(img, template, True))
