import math
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import Image

def quaternion_to_euler(orientation):
    x = orientation.x
    y = orientation.y
    z = orientation.z
    w = orientation.w
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw = math.atan2(t3, t4)
    return [yaw, pitch, roll]

def euler_to_quaternion(euler):
    # roll, pitch, yaw = euler
    yaw, pitch, roll = euler

    qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
    qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
    qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)

    return (qx, qy, qz, qw)

fuckymath = True
fov_x = 90 * math.pi / 180
fov_y = 60 * math.pi / 180
fx = 1/math.tan(fov_x/2)/2
fy = 1/math.tan(fov_y/2)/2
cx = 0.5
cy = 0.5
def calc_pose(bounding_boxes, rel_pose, threshold = 0.90, size = .2, FOV = 90, yFOV = 90, const = .8):
    msg = PoseArray()
    msg.header.frame_id = "map"
    for i, box in enumerate(bounding_boxes):
        pose = Pose()
        yMin, xMin, yMax, xMax = box

        orientation = quaternion_to_euler(rel_pose.orientation)
        camera_pitch = 30 * math.pi / 180# + orientation[1] zed too bad
        if fuckymath:
            d = size / (yMax - yMin) * fy
            # adjust for pitch

            xMid = (xMax+xMin)/2
            yMid = (yMax+yMin)/2

            x = (xMid - cx)/fx*d # x right
            dist = math.sqrt(x**2 + d**2)
            angle = math.atan2(x, d)

            # subtract angle for y left
            orientation[0] -= angle
            pose.position.x = math.cos(orientation[0])*dist
            pose.position.y = math.sin(orientation[0])*dist
        else:
            pixelPercent = (yMax-yMin)*0.95/2
            theta = pixelPercent*FOV/2*math.pi/180
            dist = size/2/math.tan(theta) * const
            angle = FOV * (xMin + (xMax - xMin) / 2) - FOV / 2

            # subtract angle for y left
            orientation[0] -= angle * math.pi / 180
            pose.position.x = math.cos(orientation[0]) * dist
            pose.position.y = math.sin(orientation[0]) * dist

        print(orientation[0])
        (pose.orientation.x, pose.orientation.y, pose.orientation.z,
                    pose.orientation.w) = euler_to_quaternion(orientation)
        # pose.orientation = rel_pose.orientation
        pose.position.x += rel_pose.position.x
        pose.position.y += rel_pose.position.y
        print(pose)
        msg.poses.append(pose)
    return msg
